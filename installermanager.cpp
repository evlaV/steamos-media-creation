#include "installermanager.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>

#include <QtDebug>

static QString kInstallerExecutable = QStringLiteral(INSTALLER_EXECUTABLE);

InstallerManager::InstallerManager(QObject *parent)
    : QObject(parent)
{
    mInstallerProcess = new QProcess();
    connect(mInstallerProcess, &QProcess::readyRead,
            this, &InstallerManager::onReadyRead);

    qDebug().noquote() << "Launching " << kInstallerExecutable;
    mInstallerProcess->start(kInstallerExecutable);
}

InstallerManager::~InstallerManager()
{
    QJsonObject object{
        { "command", "quit"}
    };

    QJsonDocument document;
    document.setObject(object);

    mInstallerProcess->write(document.toJson(QJsonDocument::Compact));
    mInstallerProcess->write("\n");
    mInstallerProcess->waitForBytesWritten();
}

void InstallerManager::requestDevices()
{
    QJsonObject object{
        { "command", "list-devices" }
    };

    QJsonDocument document;
    document.setObject(object);

    mInstallerProcess->write(document.toJson(QJsonDocument::Compact));
    mInstallerProcess->write("\n");
    mInstallerProcess->waitForBytesWritten();
}

void InstallerManager::flashImage(const QString &path, int device)
{
    if (device < 0 || device > mRemovableMediaDevices.size()) {
        emit errorMessage("Invalid device");
    }
    qDebug().noquote() << "FlashImage called, using image: " << path << " and device " << mRemovableMediaDevices.at(device);

    QJsonObject object {
        { "command", "flash-image"},
        { "path", path},
        { "device", mRemovableMediaDevices.at(device)}
    };

    QJsonDocument document;
    document.setObject(object);

    QByteArray command = document.toJson(QJsonDocument::Compact);

    qDebug().noquote() << "Sending flash command: " << command;
    mInstallerProcess->write(command);
    mInstallerProcess->write("\n");
    mInstallerProcess->waitForBytesWritten();
}

void InstallerManager::exportSettings()
{
    QJsonObject object {
        {"command", "export-settings"}
    };

    QJsonDocument document;
    document.setObject(object);

    QByteArray command = document.toJson(QJsonDocument::Compact);
    qDebug().noquote() << "Sending export settings command: " << command;
    mInstallerProcess->write(command);
    mInstallerProcess->write("\n");
    mInstallerProcess->waitForBytesWritten();
}

void InstallerManager::onReadyRead()
{
    QByteArray data = mInstallerProcess->readAll();
    qDebug().noquote() << "Read from installer: " << data;

    QJsonDocument document = QJsonDocument::fromJson(data);

    if (document.isObject()) {
        const QJsonObject object = document.object();

        if (object.contains("responsetype")) {
            QString responseType = object.value("responsetype").toString();

            if (responseType == "devicelist") {
                // Parse device list to send to gui
                qDebug() << "Parsing device list";
                QStringList deviceArray;
                mRemovableMediaDevices.clear();

                if (!object.contains("devices")) {
                    emit errorMessage("No removable media.");
                    return;
                }

                QJsonObject devicesObject = object.value("devices").toObject();

                QStringList keys = devicesObject.keys();
                for (int i = 0; i < keys.length(); ++i) {
                    QString key = keys.at(i);
                    qDebug() << "Adding device with key: " << key << " to list";
                    QJsonObject deviceObject = devicesObject.value(key).toObject();
                    deviceArray.append(deviceObject.value("description").toString());
                    // Cache device names to pass to flash-image command later
                    QString deviceString = deviceObject.value("device").toString();
                    mRemovableMediaDevices.append(deviceString);
                }

                if (deviceArray.size() == 0) {
                    emit errorMessage("No removable media.");
                    return;
                }

                qDebug() << "Found devices: " << deviceArray.size();

                emit deviceList(deviceArray);
            }
            else if (responseType == "progress") {
                // Flashing progress, update progress bar
                int value = object.value("percent").toInt();
                emit progress(value);
            } else if (responseType == "flashing") {
                // Beginning to flash, set progress bar to -1 to show spinning
                emit progress(-1);
            } else if (responseType == "error") {
                // Show error message
                emit errorMessage(object.value("message").toString());
            } else if (responseType == "settings-exported") {
                // Show done message box
                emit settingsComplete();
            } else if (responseType == "flash-complete") {
                emit flashComplete();
            }
        }
    }
}
