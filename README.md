SteamOS Installer
================



## Linux

#### Build and test on your machine

Get your dependencies (Debian-like OS):

    # To run installer.py
    sudo apt install python3-dbus python3-tzlocal

    # To build the GUI
    sudo apt install qt5-qmake qtbase5-dev 

Get your dependencies (ArchLinux):

    # To run installer.py
    sudo pacman -S python-dbus python-tzlocal

    # To build the GUI
    sudo pacman -S qt5-base

Build the GUI:

    qmake
    make

In order to run the GUI, gather everything in the same directory, ie:
`etcher-sdk-cli`, `node_modules`, `installer.py` and `steamos-media-creation`.
Then run it as root.

    sudo ./steamos-media-creation

#### Build a linux package in a docker container

    # Speed up the build with a caching proxy if needed
    export http_proxy=http://172.17.0.1:3142

    # Choose your dist among: archlinux, debian
    DIST=archlinux

    # Build the docker image
    ./buildlinux/build-docker-image.sh ${DIST:?}

    # Build the package
    ./buildlinux/run-docker-container.sh ${DIST:?}
    [in-docker]$ build-package.sh



## Windows

TODO



## macOS

TODO
