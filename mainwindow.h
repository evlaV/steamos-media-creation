#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include "installermanager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_imageLineEdit_textChanged();
    void on_browseButton_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_stackedWidget_currentChanged(int index);

    void onDeviceList(QStringList &devices);
    void onErrorMessage(QString message);
    void onProgress(int value);
    void onFlashComplete();
    void onSettingsComplete();

    void onDeviceTimerTimeout();

private:
    Ui::MainWindow *ui;

    // Enable or disable the ok button based on the
    // current page's widgets status
    void updateOkButton();

    InstallerManager *mManager;

    // True when flashing has finished so we can enable the ok button
    bool mFlashComplete;

    // True when done exporting settings to device
    bool mSettingsComplete;

    // Timer to ask installer for device list every second while on device list
    // page
    QTimer mDevicesTimer;

    // True when we have showed the error about no media
    // So we don't keep showing it over and over.
    bool mShowedNoMedia;

    // Device list
    // Used to compare incoming device list to decide whether or not
    // to repopulate the device combo box. Don't repopulate if the list
    // hasn't changed.
    QStringList m_devices;
};
#endif // MAINWINDOW_H
