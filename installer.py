#!/usr/bin/env python3
# installer.py - A simple command driven tool for creation of SteamOS
# installation media.
#
# Copyright (C) 2020-2021 Valve
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Commands:
#  - device-list: Provide list of removable media to display to user
#  - flash-image: Flash a given image (zip file) to the given device
#      parameters:
#       path: The path to the zip file to flash
#       device: The name of the device to flash to, on windows should be \\.\PhysicalDriveX
#
#  - export-settings: Export settings from current os to efi partition on removable media
#       for import into steamos calamares installation. Writes a settings folder, which contains:
#       settings.conf <-- Keyboard layout, timezone, and locale
#       network-connections <-- Folder containing one file per wifi network specifying connection settings
#
#  - steam-settings: Copy a steam settings file given by "path" argument onto the efi partition for steam
#       client on steamos to import. Filename is kept the same as it was on windows.

import argparse
import configparser
import ctypes
import ctypes.util
import datetime
import gettext
import json
import locale
import os
import re
import shlex
import shutil
import subprocess
import sys
import tempfile
import threading
import time
import vdf
from tzlocal import get_localzone
#import yaml   # not needed apparently

# Locale/gettext configuration
locale.setlocale(locale.LC_ALL, '')

# Modules and Binary name depends on the platform
if sys.platform.startswith('win32'):
    import winreg
    import wmi

    ETCHER_SDK_CLI = './etcher-sdk-cli.exe'
else:
    ETCHER_SDK_CLI = './etcher-sdk-cli'

class Configure:
    """Set up media creation by asking for image, device and finally confirming."""

    imagePath = ""
    interval = 1
    removableMedia = {}
    exiting = False
    device = ""
    flash_process = None
    scanner_process = None
    efi_mntpoint = ""    # Linux, set only if we mounted the efi partition

    def __init__(self):
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True
        self.thread.start()

    def __del__(self):
        if self.efi_mntpoint:
            try:
                self.umountLinuxPartition(self.efi_mntpoint)
                os.rmdir(self.efi_mntpoint)
            except:
                pass

    def print_test(self, commandobject):
        print ("Test output")


    def list_devices(self, commandobject):
        # List currently known devices\
        reply = {}
        reply["responsetype"] = "devicelist"
        reply["devices"] = self.removableMedia
        print (json.dumps(reply))


    def flash_image(self, commandobject):
        # flash the given image to the given device
        # Parameters: "path" - path to the image zip file to write
        # "device" - device to flash the image to
        path = commandobject.get('path')
        self.device = commandobject.get('device')
        # Construct initial reply
        reply = {}
        reply["responsetype"] = "flashing"

        if not path or not os.path.isfile(path):
            reply["responsetype"] = "error"
            reply["message"] = "flash-image requires a valid path"
            print(json.dumps(reply), flush=True)
            return

        if not self.device:
            reply["responsetype"] = "error"
            reply["message"] = "flash-image requires a valid device name to flash to"
            print(json.dumps(reply), flush=True)
            return

        # Should have a valid path and a device (may or may not be valid, we'll see)
        # TODO: Possibly use file-to-file for non windows platforms since it seems
        # to be quicker than multi-destination there.

        reply["responsetype"] = "flashing"
        reply["path"] = path
        reply["device"] = self.device
        print(json.dumps(reply), flush=True)

        del reply["path"]
        del reply["device"]

        cmd = [ETCHER_SDK_CLI, "write", path, self.device]
        self.flash_process = subprocess.Popen(cmd, stdout=subprocess.PIPE)

        processOutput = ''

        while not self.exiting:
            output = self.flash_process.stdout.readline().decode().rstrip("\n\r")
            processOutput += output
            if output == '' and self.flash_process.poll() is not None:
                break
            if output:
                percent = None
                try:
                    percent = round(float(output))
                except Exception:
                    pass

                if percent is not None:
                    reply["responsetype"] = "progress"
                    reply["percent"] = percent
                    print(json.dumps(reply), flush=True)
        rc = self.flash_process.poll()                # Process line to give progress json objects on stdout

        if rc == 0 and reply.get("percent") == 100:
            reply["responsetype"] = "flash-complete"

            if "percent" in reply:
                del reply["percent"]

            print(json.dumps(reply), flush=True)
        else:
            reply["responsetype"] = "error"
            reply["message"] = "flash failed: " + processOutput
            print(json.dumps(reply), flush=True)



    def getWindowsKeyboards(self):
        keyboards = {}
        keyboards[0x409] = 'us'
        keyboards[0x809] = 'uk'
        keyboards[0x405] = 'cz'
        keyboards[0x413] = 'nl'
        keyboards[0x813] = 'be-latin1'
        keyboards[0x40b] = 'fi'
        keyboards[0x40c] = 'fr' #French - France                0x040c
        keyboards[0x80c] = 'fr' #French - Belgium           0x080c
        keyboards[0x2c0c] = 'fr' #French - Cameroon         0x2c0c
        keyboards[0xc0c] = 'fr' #French - Canada                0x0c0c
        keyboards[0x240c] = 'fr' #French - Democratic Rep. of Congo 0x240c
        keyboards[0x300c] = 'fr' #French - Cote d'Ivoire            0x300c
        keyboards[0x3c0c] = 'fr' #French - Haiti                0x3c0c
        keyboards[0x140c] = 'fr' #French - Luxembourg           0x140c
        keyboards[0x340c] = 'fr' #French - Mali             0x340c
        keyboards[0x180c] = 'fr' #French - Monaco               0x180c
        keyboards[0x380c] = 'fr' #French - Morocco          0x380c
        keyboards[0xe40c] = 'fr' #French - North Africa         0xe40c
        keyboards[0x200c] = 'fr' #French - Reunion          0x200c
        keyboards[0x280c] = 'fr' #French - Senegal          0x280c
        keyboards[0x100c] = 'fr' #French - Switzerland          0x100c
        keyboards[0x1c0c] = 'fr' #French - West Indies          0x1c0c
        keyboards[0x407] = 'de' #German - Germany           0x407
        keyboards[0xc07] = 'de' #German - Austria           0x0c07
        keyboards[0x1407] = 'de' #German - Liechtenstein            0x1407
        keyboards[0x1007] = 'de' #German - Luxembourg           0x1007
        keyboards[0x807] = 'de' #German - Switzerland           0x807
        keyboards[0x408] = 'gr' #Greek                  0x408
        keyboards[0x410] = 'it' #Italian - Italy                0x410
        keyboards[0x810] = 'it' #Italian - Switzerland          0x810
        keyboards[0x411] = 'jp106' #Japanese                0x411
        keyboards[0x418] = 'ro' #Romanian               0x418
        keyboards[0x818] = 'ro' #Romanian - Moldava         0x818
        keyboards[0x419] = 'ru' #Russian                    0x419
        keyboards[0x419] = 'ru' #Russian - Moldava          0x819
        keyboards[0xc0a] = 'es' #Spanish - Spain (Modern Sort)      0x0c0a
        keyboards[0x40a] = 'es' #Spanish - Spain (Traditional Sort) 0x040a
        keyboards[0x2c0a] = 'es' #Spanish - Argentina           0x2c0a
        keyboards[0x400a] = 'es' #Spanish - Bolivia         0x400a
        keyboards[0x340a] = 'es' #Spanish - Chile               0x340a
        keyboards[0x240a] = 'es' #Spanish - Colombia            0x240a
        keyboards[0x140a] = 'es' #Spanish - Costa Rica          0x140a
        keyboards[0x1c0a] = 'es' #Spanish - Dominican Republic      0x1c0a
        keyboards[0x300a] = 'es' #Spanish - Ecuador         0x300a
        keyboards[0x440a] = 'es' #Spanish - El Salvador         0x440a
        keyboards[0x100a] = 'es' #Spanish - Guatemala           0x100a
        keyboards[0x480a] = 'es' #Spanish - Honduras            0x480a
        keyboards[0xe40a] = 'es' #Spanish - Latin America           0xe40a
        keyboards[0x800a] = 'es' #Spanish - Mexico          0x080a
        keyboards[0x4c0a] = 'es' #Spanish - Nicaragua           0x4c0a
        keyboards[0x180a] = 'es' #Spanish - Panama          0x180a
        keyboards[0x3c0a] = 'es' #Spanish - Paraguay            0x3c0a
        keyboards[0x280a] = 'es' #Spanish - Peru                0x280a
        keyboards[0x500a] = 'es' #Spanish - Puerto Rico         0x500a
        keyboards[0x540a] = 'es' #Spanish - United States           0x540a
        keyboards[0x380a] = 'es' #Spanish - Uruguay         0x380a
        keyboards[0x200a] = 'es' #Spanish - Venezuela           0x200a
        keyboards[0x41d] = 'sv-latin1' #Swedish                 0x041d
        keyboards[0x81d] = 'sv-latin1' #Swedish - Finland           0x081d

        return keyboards



    def getDriveBestEffort(self):
        # Iterate through removable media list to find drive letter based on
        # self.device or only removable media if there's only one.
        # Used to export settings, if we get this wrong it's not a big deal
        # since it will just write some config files to the device
        for drive in list(self.removableMedia.values()):
            if drive['device'] == self.device:
                # Found it by self.device, i.e. the device matches what we flashed onto
                return drive

        # Didn't find by device, so see if there's only one removable media
        if len(self.removableMedia) == 1:
            return list(self.removableMedia.values())[0]

        return None

    def getWindowsDriveLetter(self):
        drive = self.getDriveBestEffort()
        if drive:
            displayName = drive['displayName']
            # If there are multiple drive letters, get the first one
            if ',' in displayName:
                displayName = displayName.split(',')[0]
            if '\\\\.\\' not in displayName:
                return displayName

        return None

    def getLinuxEfiMountpoint(self):
        drive = self.getDriveBestEffort()
        if drive and 'mountpoints' in drive:
            for m in drive['mountpoints']:
                if 'label' in m and m['label'] == 'efi':
                    if 'path' in m and os.path.exists(m['path']):
                        return m['path']

        return None

    def getLinuxEfiDevice(self):
        drive = self.getDriveBestEffort()
        if not drive:
            return None

        drive = drive['device']

        # assume efi device is the 2nd partition
        efidev = drive + '2'
        if not os.path.exists(efidev):
            efidev = drive + 'p2'
        if not os.path.exists(efidev):
            return None

        return efidev

    def mountLinuxPartition(self, source, target, fs, options=''):
        libc = ctypes.CDLL(ctypes.util.find_library('c'), use_errno=True)
        libc.mount.argtypes = (ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_ulong, ctypes.c_char_p)

        ret = libc.mount(source.encode(), target.encode(), fs.encode(), 0, options.encode())
        if ret < 0:
            errno = ctypes.get_errno()
            raise OSError(errno, "Error mounting {} ({}) on {} with options '{}': {}".format(
                source, fs, target, options, os.strerror(errno)))

    def umountLinuxPartition(self, mntpoint):
        libc = ctypes.CDLL(ctypes.util.find_library('c'), use_errno=True)
        libc.umount.argtypes = (ctypes.c_char_p, )

        ret = libc.umount(mntpoint.encode())
        if ret < 0:
            errno = ctypes.get_errno()
            raise OSError(errno, "Error umounting {}: {}".format(mntpoint, os.strerror(errno)))

    def getSteamPath(self):
        if (sys.platform.startswith('win32')):
            with winreg.OpenKey(winreg.HKEY_CURRENT_USER, "Software\\Valve\\Steam") as regkey:
                return winreg.QueryValueEx(regkey, "SteamPath")[0]

        return None

    def getVolumeInfo(self, driveletter):
        for vol in wmi.WMI().Win32_Volume():
            if vol.Driveletter and vol.DriveLetter == driveletter.upper():
                filesystem = vol.FileSystem
                if (filesystem == "NTFS"):
                    filesystem = "lowntfs-3g"
                elif (filesystem == "FAT32"):
                    filesystem = "vfat"
                else:
                    filesystem = "auto"

                return (re.findall(r'\\\\\?\\Volume{(.*)}\\', vol.DeviceID)[0], filesystem)

        return None, None

    def export_settings(self, commandobject):
        # Get the current os settings and write to .conf files for calamares
        # on efi partition

        # Get efi partition drive letter from scanner output
        if (sys.platform.startswith('win32')):
            # Get the drive letter from scanner device list
            driveletter = self.getWindowsDriveLetter()

            if driveletter is None:
                # Failed to get drive letter by self.device or only removable media
                reply = {}
                reply["responsetype"] = "error"
                reply["message"] = "Unknown drive letter"

                print(json.dumps(reply), flush=True)
                return

            # Get timezone
            tz = get_localzone()
            zonename = tz.zone

            # Get system locale
            windll = ctypes.windll.kernel32
            systemlocale = locale.windows_locale[windll.GetUserDefaultUILanguage()]
            systemlocale += '.UTF-8'

            # Get system keyboard layout and variant
            user32 = ctypes.windll.user32
            klid = user32.GetKeyboardLayout(0) & 0xFFFF

            keyboards = self.getWindowsKeyboards()

            if klid in keyboards:
                keyboard = keyboards[klid]
            else:
                keyboard = "us"

            # Create the settings folder
            try:
                os.mkdir(driveletter + "/settings")
            except FileExistsError:
                pass

            # Put settings in settings.conf file
            stream = open(driveletter + '/settings/settings.conf', 'w', newline='\n')
            stream.write("LOCALE=" + systemlocale + "\n")
            stream.write("KEYBOARD=" + keyboard + "\n")
            stream.write("TZ=" + zonename + "\n")

            # Get Steam Path
            steamPath = self.getSteamPath()
            if steamPath:
                options = ["defaults", "nofail", "ro" , "umask=0002" , "uid=1000", "gid=1000", "ignore_case"]

                with open(driveletter + '/settings/steamlibtab', 'w', newline='\n') as fstab:
                    fstab.write("# Static information about the steam libraries.\n")
                    fstab.write("# See fstab(5) for details.\n\n")
                    fstab.write("# <file system> <dir> <type> <options> <dump> <pass>\n")

                    index = 0
                    drive, path = os.path.splitdrive(steamPath)
                    path = path.replace("\\", "/")
                    uuid, filesystem = self.getVolumeInfo(drive)
                    if uuid:
                        opts = options + ["x-steamos.steamlib=" + path.replace(" ", "\\040")]
                        fstab.write("# %s\n" % steamPath)
                        fstab.write("PARTUUID=%s /run/steam/%s %s %s 0 0\n" % (uuid, index, filesystem, ",".join(opts)))
                    else:
                        fstab.write("# %s not found!\n" % steamPath)

                    config = vdf.load(open(steamPath + "/config/config.vdf"))
                    steam = config['InstallConfigStore']['Software']['Valve']['Steam']
                    for key in steam:
                        if key.startswith('BaseInstallFolder_'):
                            index = key[18:]
                            drive, path = os.path.splitdrive(steam[key])
                            path = path.replace("\\", "/")
                            uuid, filesystem = self.getVolumeInfo(drive)
                            if uuid:
                                opts = options + ["x-steamos.steamlib=" + path.replace(" ", "\\040")]
                                fstab.write("# %s\n" % steam[key])
                                fstab.write("PARTUUID=%s /run/steam/%s %s %s 0 0\n" % (uuid, index, filesystem, ",".join(opts)))
                            else:
                                fstab.write("# %s not found!\n" % steam[key])

            # Now export wifi passwords
            # Create the network-connections folder
            try:
                os.mkdir(driveletter + "/settings/network-connections")
            except FileExistsError:
                pass

            # First get all wifi networks known names
            try:
                output = subprocess.check_output(['netsh', 'wlan', 'show', 'profiles']).decode()

                # Parse output for wlan profiles
                lines = output.split('\n')

                wifiProfiles = {}

                foundProfiles = False

                for line in lines:
                    line = line.rstrip("\n")
                    if ('User profiles' in line):
                        foundProfiles = True

                    if (foundProfiles and ':' in line):
                        words = line.split(':')
                        key = words[len(words) - 1].rstrip('\n\r').strip()
                        wifiProfiles[key] = {}

                # Get password for each profile with netsh wlan show profile name="network-profile-name" key=clear
                for profile in wifiProfiles.keys():
                    output = subprocess.check_output(['netsh', 'wlan', 'show', 'profile', 'name="' + profile +'"', 'key=clear']).decode()

                    profilelines = output.split('\n')
                    # default to wpa-psk
                    auth = 'wpa-psk'

                    for line in profilelines:
                        line = line.rstrip("\n")
                        if ('Key Content' in line):
                            words = line.split(':')
                            key = words[len(words) - 1].rstrip('\n\r').strip()

                        if ('Authentication' in line):
                            words = line.split(':')
                            win_auth = words[len(words) - 1].rstrip('\n\r').strip()
                            if (win_auth == 'WPA2-Personal'):
                                auth = 'wpa-psk'

                    # Write network manager conf file for this wifi profile
                    config = configparser.ConfigParser()
                    # connection group
                    config['connection'] = {}
                    config['connection']['id'] = profile
                    config['connection']['type'] = 'wifi'
                    config['connection']['autoconnect'] = 'true'
                    # ignoring UUID and permissions for now

                    # wifi group
                    config['wifi'] = {}
                    config['wifi']['ssid'] = profile
                    config['wifi']['mode'] = 'infrastructure'
                    # ignoring mac-address, mac-address-blacklist

                    # wifi-security
                    config['wifi-security'] = {}
                    # TODO: Make this depend on the details found from netsh
                    config['wifi-security']['key-mgmt'] = auth
                    config['wifi-security']['psk'] = key

                    # ipv4
                    config['ipv4'] = {}
                    config['ipv4']['dns-search'] = ''
                    config['ipv4']['method'] = 'auto'

                    #ipv6
                    config['ipv6'] = {}
                    config['ipv6']['dns-search'] = ''
                    config['ipv6']['method'] = 'auto'
                    config['ipv6']['ip6-privacy'] = '0'
                    config['ipv6']['addr-gen-mode'] = 'stable-privacy'

                    with open(driveletter + '/settings/network-connections/' + profile, 'w', newline='\n') as configfile:
                        config.write(configfile)
            except:
                pass

        elif (sys.platform.startswith('darwin')):
            # TODO: macos specific settings export code goes here
            pass

        elif (sys.platform.startswith('linux')):

            def print_dict(d):
                output = ''
                for key, val in d.items():
                    output += '{}={}\n'.format(key, val)
                return output

            def get_user_settings():
                settings = {}

                # Get timezone
                tz = get_localzone()
                settings['TZ'] = tz.zone
                
                # Get locale details via dbus
                try:
                    import dbus
                except ImportError:
                    return settings

                LOCALE1 = 'org.freedesktop.locale1'
                LOCALE1_PATH = '/org/freedesktop/locale1'
                DBUS_PROPS = 'org.freedesktop.DBus.Properties'

                bus = dbus.SystemBus()
                locale1 = None
                try:
                    locale1 = bus.get_object(LOCALE1, LOCALE1_PATH)
                except dbus.exceptions.DBusException:
                    return settings

                locale = locale1.Get(LOCALE1, 'Locale', dbus_interface=DBUS_PROPS)
                if locale:
                    settings['LOCALE'] = locale[0]

                x11layout = locale1.Get(LOCALE1, 'X11Layout', dbus_interface=DBUS_PROPS)
                x11variant = locale1.Get(LOCALE1, 'X11Variant', dbus_interface=DBUS_PROPS)
                if x11variant:
                    settings['KEYBOARD'] = x11layout + ':' + x11variant
                else:
                    settings['KEYBOARD'] = x11layout

                #x11model = locale1.Get(LOCALE1, 'X11Model', dbus_interface=DBUS_PROPS)

                return settings

            def get_wifi_ssid():

                try:
                    import dbus
                except ImportError:
                    return None

                NM = 'org.freedesktop.NetworkManager'
                NM_PATH = '/org/freedesktop/NetworkManager'
                NMCA = NM + '.Connection.Active'
                NMDW = NM + '.Device.Wireless'
                NMAP = NM + '.AccessPoint'
                DBUS_PROPS = 'org.freedesktop.DBus.Properties'

                bus = dbus.SystemBus()
                nm = None
                try:
                    nm = bus.get_object(NM, NM_PATH)
                except dbus.exceptions.DBusException:
                    return None

                conns = nm.Get(NM, 'ActiveConnections', dbus_interface=DBUS_PROPS)
                for conn in conns:
                    try:
                        active_conn = bus.get_object(NM, conn)
                        devices = active_conn.Get(NMCA, 'Devices',
                                dbus_interface=DBUS_PROPS)
                        dev_name = devices[0]
                        dev = bus.get_object(NM, dev_name)
                        ap_name = dev.Get(NMDW, 'ActiveAccessPoint',
                                dbus_interface=DBUS_PROPS)
                        ap = bus.get_object(NM, ap_name)
                        ssid = ap.Get(NMAP, 'Ssid', dbus_interface=DBUS_PROPS,
                                byte_arrays=True)
                        if ssid:
                            return ssid.decode('utf-8')
                    except dbus.exceptions.DBusException:
                        continue

                return None

            # Get output directory - be a bit patient, as we don't know if the
            # desktop environment is about to mount the efi partition automatically,
            # after SteamOS was written to the USB media.
            exportdir = None
            for retry in range(5):
                exportdir = self.getLinuxEfiMountpoint()
                if exportdir:
                    break
                time.sleep(1)

            if exportdir is None:
                efidev = self.getLinuxEfiDevice()
                if not efidev:
                    reply = {}
                    reply["responsetype"] = "error"
                    reply["message"] = "Could not find efi device"
                    print(json.dumps(reply), flush=True)
                    return

                efimnt = tempfile.mkdtemp()
                try:
                    self.mountLinuxPartition(efidev, efimnt, 'vfat')
                except OSError as e:
                    reply = {}
                    reply["responsetype"] = "error"
                    reply["message"] = "Could not mount efi device: {}".format(e)
                    print(json.dumps(reply), flush=True)
                    return

                exportdir = efimnt
                self.efi_mntpoint = efimnt

            exportdir = os.path.join(exportdir, 'settings')

            # Get user settings
            settings = get_user_settings()
            if settings:
                output = print_dict(settings)

                try:
                    os.mkdir(exportdir)
                except FileExistsError:
                    pass

                outfile = os.path.join(exportdir, 'settings.conf')
                with open(outfile, 'w') as f:
                    f.write(output)

            # Get wifi settings
            ssid = get_wifi_ssid()
            if ssid:
                filenames = [ ssid + '.nmconnection', ssid ]
                filename = None
                for f in filenames:
                    f = '/etc/NetworkManager/system-connections/' + f
                    if os.path.exists(f):
                        filename = f
                        break

                if filename:
                    with open(filename, 'r') as f:
                        lines = f.readlines()

                    # under the [wifi] section, this is the mac address of the client
                    lines = [ x for x in lines if not x.startswith('mac-address=') ]

                    outdir = os.path.join(exportdir, 'network-connections')
                    try:
                        os.mkdir(outdir)
                    except FileExistsError:
                        pass

                    outfile = os.path.join(outdir, os.path.basename(filename))
                    with open(outfile, 'w') as f:
                        f.write(''.join(lines))

            # Might want to cleanup
            if self.efi_mntpoint:
                try:
                   self.umountLinuxPartition(self.efi_mntpoint)
                   os.rmdir(self.efi_mntpoint)
                   self.efi_mntpoint = ""
                except:
                    pass

        # Construct json response
        reply = {}
        reply["responsetype"] = "settings-exported"

        print(json.dumps(reply), flush=True)


    def steamos_settings(self, commandobject):
        path = commandobject.get('path')
        destination = None
        if not path or not os.path.isfile(path):
            reply = {}
            reply["responsetype"] = "error"
            reply["message"] = "missing or invalid steam settings 'path'"
            print(json.dumps(reply), flush=True)
            return 

        # Construct default reply
        reply = {}
        reply["responsetype"] = "settings-copied"

        if (sys.platform.startswith('win32')):
            # Get the drive letter from scanner device list
            driveletter = self.getWindowsDriveLetter()

            if driveletter is None:
                # Failed to get drive letter by self.device or only removable media
                reply = {}
                reply["responsetype"] = "error"
                reply["message"] = "Unknown drive letter"

                print(json.dumps(reply), flush=True)
                return

            os.mkdir(os.path.join(driveletter, "steam-settings"))
            destination = os.path.join(driveletter, "steam-settings", os.path.basename(path))
            
        elif (sys.platform.startswith('linux')):
            # Get output directory - be a bit patient, as we don't know if the
            # desktop environment is about to mount the efi partition automatically,
            # after SteamOS was written to the USB media.
            exportdir = None
            for retry in range(5):
                exportdir = self.getLinuxEfiMountpoint()
                if exportdir:
                    break
                time.sleep(1)

            if exportdir is None:
                efidev = self.getLinuxEfiDevice()
                if not efidev:
                    reply = {}
                    reply["responsetype"] = "error"
                    reply["message"] = "Could not find efi device"
                    print(json.dumps(reply), flush=True)
                    return

                efimnt = tempfile.mkdtemp()
                try:
                    self.mountLinuxPartition(efidev, efimnt, 'vfat')
                except OSError as e:
                    reply = {}
                    reply["responsetype"] = "error"
                    reply["message"] = "Could not mount efi device: {}".format(e)
                    print(json.dumps(reply), flush=True)
                    return

                exportdir = efimnt
                self.efi_mntpoint = efimnt
                os.mkdir(os.path.join(exportdir, "steam-settings"))
                destination = os.path.join(exportdir, "steam-settings", os.path.basename(path))

        try: 
            shutil.copyfile(path, destination)
            reply["path"] = destination
        
        # If source and destination are same 
        except shutil.SameFileError:
            reply["responsetype"] = "error"
            reply["message"] = "failed to copy steam settings, files are the same path"
        
        # If destination is a directory. 
        except IsADirectoryError:
            reply["responsetype"] = "error"
            reply["message"] = "failed to copy steam settings, target is a directory"
        
        # If there is any permission issue 
        except PermissionError: 
            reply["responsetype"] = "error"
            reply["message"] = "failed to copy steam settings, permission error"
        
        # For other errors 
        except:
            reply["responsetype"] = "error"
            reply["message"] = "failed to copy steam settings, generic exception"

        print(json.dumps(reply), flush=True)


    def quit(self, commandobject):
        if self.scanner_process:
            self.scanner_process.kill()
        if self.flash_process:
            self.flash_process.kill()
        self.exiting = True
        self.thread.join()
        sys.exit(0)


    commands = {
        "test": print_test,
        "list-devices": list_devices, # List available removable media devices
        "flash-image": flash_image, # flash given image to given device
        "export-settings": export_settings, # export settings to efi partition files
        "steam-settings": steamos_settings, # Copy steam settings file to efi partition for use on steamos
        "quit": quit,
        }

    def processCommands(self):
        # Read commands from stdin,
        # Report results and events on stdout
        # Used when interacting with software when --json argument is used
        while True:
            command = input("")

            try:
                commandobject = json.loads(command)

                commandtype = commandobject.get('command')
                if (commandtype and commandtype in self.commands):
                    # Perform the given command
                    self.commands.get(commandtype)(self, commandobject)
            except Exception as e:
                print ('Invalid command: {}: {}'.format(command, e))


    def run(self):
        # Run scanner populating device list as we are notified of removable media

        cmd = [ETCHER_SDK_CLI, "scan"]

        self.scanner_process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        output = ""
        # True if we are getting an attach event, false for detach
        eventAttach = True
        while not self.exiting:
            line = self.scanner_process.stdout.readline()
            if line == b'' and self.scanner_process.poll() is not None:
                break
            if line:
                if line.startswith(b'attach'):
                    eventAttach = True
                    line = line[6:]
                elif line.startswith(b'detach'):
                    eventAttach = False
                    line = line[6:]
                elif line.startswith(b'ready'):
                    # Ignore lines that start with 'ready'
                    line = b''

                output += line.decode('ASCII')
                try:
                    object = json.loads(output)

                    # Valid json, so clear output buffer for next event
                    output = ""

                    if (object["drive"]["isRemovable"]):
                        displayName = object["drive"]["displayName"]
                        if (eventAttach):
                            self.removableMedia[displayName] = object["drive"]
                        else:
                            del self.removableMedia[displayName]
                    else:
                        pass
                except Exception:
                    # Do nothing, output is invalid, keep waiting
                    pass


def main():
    args = argparse.Namespace()

    ourdescription = '''SteamOS Media Creation.
        Does the work of running etcher-sdk-cli to get removable media, flash the zip file to the media, then export user settings onto the media for import.
        
        simple command line utility that works by receiving json commands and giving json responses on stdout. Here are the commands it takes:

        {"command": "list-devices"}

        {"command": "flash-image", "path": "c:/some/path/to/latest/image.img.zip", "device": "\\\\.\\PhysicalDriveX"}

        {"command": "export-settings"}

        {"command": "quit"}

        {"command": "steam-settings", "path": "c:/some/steamclient/settings.ini"}

        Responses all include a "responsetype" indicating one of the following:

        "error" -- A "message" indicates what the error is, usually to show the user.

        "progress" -- Flashing progress with a "percent" value between 0 and 100.

        "settings-exported" -- Indicator that settings export is complete.

        "flashing" -- Indicator that flashing has begun.

        "flash-complete" -- Indicator that flashing is done.

        "devicelist" -- "devices" is a json list of devices with their attributes including their "device" which is passed to "flash-image" command.

        "settings-copied" -- steam-settings command has completed copying the given path onto the efi partition.'''
    common_parser = argparse.ArgumentParser(description=ourdescription, formatter_class=argparse.RawDescriptionHelpFormatter)

    common_parser.parse_args(namespace=args)

    # Using json format for input and output
    configure = Configure()
    configure.processCommands()

if __name__ == "__main__":
    sys.exit(main())
