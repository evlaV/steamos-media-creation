#ifndef INSTALLERMANAGER_H
#define INSTALLERMANAGER_H

#include <QObject>

// This class manages running installer.exe to perform installation
// of steamos images onto removable media.
// It runs in a separate thread than the gui and signals/slots are used to
// communicate with it.

class QProcess;

class InstallerManager : public QObject
{
    Q_OBJECT
public:
    explicit InstallerManager(QObject *parent = nullptr);
    ~InstallerManager();

    // Ask installer.exe for removable media to show
    void requestDevices();

    // Ask installer.exe to flash given path to given removable media
    void flashImage(const QString &path, int device);

    // Export settings from current OS to the media for import
    void exportSettings();

signals:
    // An error occurred, show the message
    void errorMessage(QString message);

    // Device list parsed from installer list-devices json output
    void deviceList(QStringList &devices);

    // Show flashing progress with given value 100 is complete
    void progress(int progress);

    // Sent when flash finishes
    void flashComplete();

    // Sent when done exporting settings
    void settingsComplete();

private slots:
    void onReadyRead();

private:
    QProcess *mInstallerProcess;

    // List of removable media devices to look up when flashImage is called
    QStringList mRemovableMediaDevices;
};

#endif // INSTALLERMANAGER_H
