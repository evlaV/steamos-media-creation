from cx_Freeze import setup, Executable 
  
build_exe_options = {
    "build_exe": ".\\buildwindows\\packages\\etcher.sdk\\data\\"
}

setup(name = "installer" , 
    options = {'build_exe': build_exe_options},
    version = "0.1",
    description = "SteamOS media creation tool for windows",
    executables = [Executable("installer.py")]
) 
