#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QStandardPaths>

#include <QDebug>

const int kImagePage = 0;
const int kDevicePage = 1;
const int kProgressPage = 2;
const int kSettingsPage = 3;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      mManager(nullptr),
      mFlashComplete(false),
      mSettingsComplete(false),
      mShowedNoMedia(false)
{
    ui->setupUi(this);
    // Make OK button disabled to start until an image is chosen
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    mManager = new InstallerManager(this);
    connect(mManager, &InstallerManager::deviceList,
            this, &MainWindow::onDeviceList);
    connect(mManager, &InstallerManager::errorMessage,
            this, &MainWindow::onErrorMessage);
    connect(mManager, &InstallerManager::progress,
            this, &MainWindow::onProgress);
    connect(mManager, &InstallerManager::flashComplete,
            this, &MainWindow::onFlashComplete);
    connect(mManager, &InstallerManager::settingsComplete,
            this, &MainWindow::onSettingsComplete);

    connect(&mDevicesTimer, &QTimer::timeout,
            this, &MainWindow::onDeviceTimerTimeout);
}

MainWindow::~MainWindow()
{
    delete mManager;
    mManager = nullptr;

    delete ui;
}

void MainWindow::on_imageLineEdit_textChanged()
{
    updateOkButton();
}

void MainWindow::on_browseButton_clicked()
{
    QString downloadPath = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
    QString newPath =
            QFileDialog::getOpenFileName(this, "Select steamos image file",
                                         downloadPath,
                                         "Image file (*.iso, *.gz, *.zip)");
    if (!newPath.isEmpty()) {
        ui->imageLineEdit->setText(newPath);
    }
}

void MainWindow::on_buttonBox_accepted()
{
    int currentPage = ui->stackedWidget->currentIndex();
    if (currentPage < kSettingsPage) {
        ui->stackedWidget->setCurrentIndex(currentPage + 1);
        updateOkButton();
    }

    if (currentPage == kSettingsPage) {
        qApp->quit();
    }
}

void MainWindow::on_buttonBox_rejected()
{
    int currentPage = ui->stackedWidget->currentIndex();
    if (currentPage > 0 && currentPage != kSettingsPage) {
        ui->stackedWidget->setCurrentIndex(currentPage - 1);
        updateOkButton();
    }
    else // Cancel was clicked on the first page
        qApp->quit();
}

void MainWindow::on_stackedWidget_currentChanged(int index)
{
    switch(index) {
    case kImagePage:
        // Nothing to do.
        mDevicesTimer.stop();
        break;
    case kDevicePage:
        mManager->requestDevices();
        mDevicesTimer.start(1000); // Request devices every second while on this page
        break;
    case kProgressPage:
        mDevicesTimer.stop();
        mManager->flashImage(ui->imageLineEdit->text(), ui->deviceComboBox->currentIndex());
        break;
    case kSettingsPage:
        mManager->exportSettings();
        break;
    }
}

void MainWindow::onDeviceList(QStringList &devices)
{
    if (devices != m_devices) {
        // Add all devices to the combo box
        ui->deviceComboBox->clear();
        ui->deviceComboBox->addItems(devices);
        m_devices = devices;

        updateOkButton();
    }
}

void MainWindow::onErrorMessage(QString message)
{
    // Show a message box with the error message
    if (message == "No removable media.") {
        if (mShowedNoMedia)
            return;
        else
            // Set flag so we don't show this message again
            mShowedNoMedia = true;
    }
    QMessageBox::critical(this, "Error", message);
}

void MainWindow::onProgress(int value)
{
    ui->progressBar->setValue(value);
}

void MainWindow::onFlashComplete()
{
    mFlashComplete = true;
    updateOkButton();
}

void MainWindow::onSettingsComplete()
{
    mSettingsComplete = true;
    updateOkButton();

    QMessageBox::information(this, "Complete", "Done exporting settings, reboot into device to start SteamOS");
}

void MainWindow::onDeviceTimerTimeout()
{
    mManager->requestDevices();
}

void MainWindow::updateOkButton()
{
    int currentPage = ui->stackedWidget->currentIndex();

    bool enable = false;

    qDebug() << "updateOkButton called, current page is " << currentPage;

    switch (currentPage) {
        case kImagePage:
        {
            // Check if the path is a valid image file
            QString path = ui->imageLineEdit->text();
            qDebug() << "Checking if " << path << " exists";

            // If so, enable the ok button to move to the
            // next page.
            enable = QFileInfo::exists(path);
        }
            break;
        case kDevicePage:
            // Disable while no valid device is selected.
            enable = (ui->deviceComboBox->count() > 0 && ui->deviceComboBox->currentIndex() >= 0);
            break;
        case kProgressPage:
            // Disable while we are flashing the image and performing settings
            // steps. Enable once done
            enable = mFlashComplete;
            break;
        case kSettingsPage:
            // Disable while exporting settings, enable when done.
            enable = mSettingsComplete;
            break;
    }

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(enable);
}

