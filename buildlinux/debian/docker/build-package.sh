#!/bin/bash

set -eu

[ -d debian ] || exit 1
[ -d build-debian ] || exit 1

dpkg-buildpackage -B -us -uc
mv ../*.{buildinfo,changes,deb} build-debian
dh_clean
