# This script generates a linux release of installer.py, requirements.txt, README.installer and etcher
# Including:
# 1. Copy base files into place
# 2. Zip up packages/etcher.sdk/data/* in a linux-steamos-installer.zip with options to keep executables executable.

cp ../installer.py etcher-sdk/
cp ../requirements.txt etcher-sdk/
cp ../README.installer etcher-sdk/

cd etcher-sdk/
# Put the zip at the top level inside steamos-media-creation
zip ../../linux-steamos-installer.zip *
