#!/bin/bash
# vim: et sts=4 sw=4

# Usage: $0 DIR
# Usage: http_proxy=http://172.17.0.1:3142 $0 DIR

set -e
set -u

DIST=${1%/}
shift

DOCKER=docker
DOCKER_ARGS=()
DOCKER_IMAGE=$DIST/steamos-media-creation-builder


## helpers

fail() { echo >&2 "$@"; exit 1; }
user_in_docker_group() { id -Gn | grep -q '\bdocker\b'; }


## main

[ -d "buildlinux" ] || fail "Please run from the root of the source tree"
[ -d "buildlinux/$DIST" ] || fail "Invalid '$DIST'"

rm -fr "build-$DIST"
mkdir  "build-$DIST"

DOCKER_ARGS+=(-u $(id -u):$(id -g))
DOCKER_ARGS+=(-v "/etc/passwd:/etc/passwd:ro")
DOCKER_ARGS+=(-v "/etc/group:/etc/group:ro")
DOCKER_ARGS+=(-v "$(pwd):/tmp/steamos-media-creation")

case "$DIST" in
    (archlinux)
        cp buildlinux/archlinux/PKGBUILD build-$DIST/
        DOCKER_ARGS+=(-w "/tmp/steamos-media-creation/build-$DIST")
        ;;

    (debian)
        DOCKER_ARGS+=(-v "$(pwd)/buildlinux/debian:/tmp/steamos-media-creation/debian")
        DOCKER_ARGS+=(-w "/tmp/steamos-media-creation")
        trap "rmdir debian" EXIT
        ;;

    (*)
        exit 1
        ;;
esac

[ "${http_proxy:-}" ] && \
    DOCKER_ARGS+=(-e "http_proxy=$http_proxy")

user_in_docker_group || \
    DOCKER='sudo docker'

$DOCKER run -it --rm \
    "${DOCKER_ARGS[@]}" \
    $DOCKER_IMAGE \
    "$@"
