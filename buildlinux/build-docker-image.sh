#!/bin/bash
# vim: et sts=4 sw=4

# Usage: $0 DIR
# Usage: http_proxy=http://172.17.0.1:3142 $0 DIR

set -e
set -u

DIST=${1%/}
shift

DOCKER=docker
DOCKER_ARGS=()
DOCKER_IMAGE=$DIST/steamos-media-creation-builder


## helpers

fail() { echo >&2 "$@"; exit 1; }
user_in_docker_group() { id -Gn | grep -q '\bdocker\b'; }


## main

[ -d "buildlinux" ] || fail "Please run from the root of the source tree"
[ -d "buildlinux/$DIST" ] || fail "Invalid '$DIST'"

cd "buildlinux/$DIST"

[ "${http_proxy:-}" ] && \
    DOCKER_ARGS+=(--build-arg "http_proxy=$http_proxy")

user_in_docker_group || \
    DOCKER='sudo docker'

$DOCKER build \
    "${DOCKER_ARGS[@]}" \
    --tag "$DOCKER_IMAGE" \
    .
