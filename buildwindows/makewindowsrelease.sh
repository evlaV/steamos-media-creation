# Use this script to generate a windows release for valve.
# This script does 3 things:
# 1 run setup.py to generate installer.exe from installer.py
# 2 run makeadmin.sh to mark installer.exe to run as admin
# 3 zip up the result for release
#
# Requirements:
# Run this script from a windows git bash prompt
# 7zip must be installed and 7z.exe in user's path
# Python 3 must be installed and python3 in user's path

cd ../
python3 ./setup.py build

cd buildwindows
./makebinariesadmin.sh

cd packages/etcher.sdk/data/
7z a ../../../../windows-steamos-installer.zip *
