# In order to make windows executable files run as admin
# we need to use the mt.exe tool from one of the windows sdks.
# In order to run this script run it from a shell that has mt.exe
# in the path.

MTPATH="C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x86\mt.exe"

"$MTPATH" -manifest runasadmin.manifest -outputresource:"packages/etcher.sdk/data/installer.exe;1"
"$MTPATH" -manifest runasadmin.manifest -outputresource:"packages/media.creation/data/steamos-media-creation.exe;1"

echo "Checking manifests"
"$MTPATH" -inputresource:"packages/etcher.sdk/data/installer.exe;1" -out:con
"$MTPATH" -inputresource:"packages/media.creation/data/steamos-media-creation.exe;1" -out:con
